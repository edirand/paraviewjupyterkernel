#ifndef pqJupyterPluginTraceReactions_h
#define pqJupyterPluginTraceReactions_h

#include <pqReaction.h>

#include <QScopedPointer>

class pqJupyterPluginTraceReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqJupyterPluginTraceReaction(QAction* p);
  ~pqJupyterPluginTraceReaction() override;

protected slots:
  void onTriggered() override;
  void onStandardAction();

  void updateAction();
  void initialize();

private:
  class pqInternals;
  friend class pqInternals;
  QScopedPointer<pqInternals> Internals;
};

#endif
