
#include "pqJupyterPluginActions.h"

#include "pqJupyterPluginTraceReaction.h"

#include <QApplication>
#include <QStyle>

//-----------------------------------------------------------------------------
pqJupyterPluginActions::pqJupyterPluginActions(QObject* p)
  : QActionGroup(p)
{
  QIcon icon = qApp->style()->standardIcon(QStyle::SP_DialogNoButton);
  QAction* a = this->addAction(new QAction(icon, "Record Trace", this));

  new pqJupyterPluginTraceReaction(a);
}
