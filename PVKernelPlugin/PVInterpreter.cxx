/*=========================================================================

   Program: ParaView
   Module:    PVInterpreter.cxx

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include "PVInterpreter.h"

// xeus includes
#include "xeus/xguid.hpp"

// paraview includes
#include "pqCoreUtilities.h"
#include "pqImageUtil.h"
#include "pqPythonShellCompleter.h"
#include "pqSaveScreenshotReaction.h"

#include "vtkCommand.h"
#include "vtkImageData.h"
#include "vtkPythonInteractiveInterpreter.h"

// std includes
#include <iostream>

// qt includes
#include <QBuffer>
#include <QByteArray>
#include <QRegularExpression>

namespace
{
QByteArray getRawImage(const QSize& size, bool all_views)
{
  vtkSmartPointer<vtkImageData> image = pqSaveScreenshotReaction::takeScreenshot(size, all_views);
  if (!image)
  {
    return QByteArray();
  }

  QImage qimg;
  pqImageUtil::fromImageData(image, qimg);

  QByteArray bArray;
  QBuffer buff(&bArray);
  qimg.save(&buff, "PNG");

  return bArray;
}
}

//------------------------------------------------------------------------------
PVInterpreter::PVInterpreter()
{
  xeus::register_interpreter(this);
  m_interpreter = vtkSmartPointer<vtkPythonInteractiveInterpreter>::New();
  m_completer = new pqPythonShellCompleter(nullptr, m_interpreter);
}

//------------------------------------------------------------------------------
void PVInterpreter::configure_impl()
{
  auto handle_comm_opened = [](xeus::xcomm&& comm, const xeus::xmessage&) {
    std::cout << "Comm opened for target: " << comm.target().name() << std::endl;
  };
  comm_manager().register_comm_target("echo_target", handle_comm_opened);
  m_interpreter->AddObserver(vtkCommand::AnyEvent, this, &PVInterpreter::handle_events);
}

//------------------------------------------------------------------------------
void PVInterpreter::handle_events(
  vtkObject* vtkNotUsed(caller), unsigned long eventId, void* callData)
{
  if (eventId == vtkCommand::ErrorEvent)
  {
    auto error_message = reinterpret_cast<char*>(callData);
    m_error_messages.push_back(error_message);
  }
  else if (eventId == vtkCommand::SetOutputEvent)
  {
    auto output_message = reinterpret_cast<char*>(callData);
    m_output_messages.push_back(output_message);
  }
}

//------------------------------------------------------------------------------
void PVInterpreter::initialize_python()
{
  m_interpreter->Push("from paraview.simple import *");
}

//------------------------------------------------------------------------------
bool PVInterpreter::push_to_new_cell(const std::string& code)
{
  // js API can be found here:
  // https://github.com/jupyter/notebook/blob/76a323e677b7080a1e9a88437d6b5cea6cc0403b/notebook/static/notebook/js/notebook.js
  std::string jsCode = "var c = Jupyter.notebook.insert_cell_below('code');\nc.set_text(\"";
  jsCode += code;
  jsCode += "\")";
  nl::json disp_data;
  disp_data["application/javascript"] = jsCode;
  display_data(disp_data, nl::json({}), nl::json({}));
  return true;
}

//------------------------------------------------------------------------------
nl::json PVInterpreter::execute_request_impl(int execution_counter, const std::string& code,
  bool silent, bool vtkNotUsed(store_history), nl::json vtkNotUsed(user_expressions),
  bool vtkNotUsed(allow_stdin))
{
  // clean up buffers
  m_error_messages.clear();
  m_output_messages.clear();

  nl::json result;

  // Parse lines of code from cell
  QString qscode = QString::fromUtf8(code.c_str());
  // add new line at the end of block to ensure end of scope.
  qscode.append("\n");

  /**
   * Defines Display method. It is equivalent to this python declaration:
   * ```
   * def Display(w = 400, h = 300, all_views = False):
   * ```
   * We need to catch it here to call the `display_data` method.
   */
  QRegularExpression screenshotCommand("^Display\\(((\\d+),(\\d+)(,(\\w+))?)?\\)");

  if (!qscode.isEmpty())
  {
    auto lines = qscode.split('\n');
    for (auto line : lines)
    {
      QRegularExpressionMatch sshotMatch = screenshotCommand.match(line.simplified().remove(" "));
      if (sshotMatch.hasMatch())
      {
        QString width = sshotMatch.captured(2);
        QString height = sshotMatch.captured(3);
        QString allViews = sshotMatch.captured(5);

        QSize size = width.isEmpty() ? QSize(400, 300) : QSize(width.toInt(), height.toInt());
        bool all = allViews.toLower().compare("true") == 0;
        nl::json disp_data;
        disp_data["image/png"] = getRawImage(size, all).toBase64().toStdString();
        display_data(disp_data, nl::json({}), nl::json({}));
      }
      else
      {
        m_interpreter->Push(line.toStdString().c_str());
      }
    }
  }

  nl::json pub_data;
  result["status"] = "ok";
  if (!m_error_messages.empty())
  {
    result["status"] = "error";
    result["ename"] = "ParaView error";
    result["evalue"] = "ParaView failed to interpret input";
    std::string message = "Error occurred in ParaView:\n";
    for (auto msg : m_error_messages)
    {
      message += std::string(msg);
    }
    if (!silent)
    {
      publish_execution_error(
        "Error", "Paraview failed to interpret intput code", m_error_messages);
    }
  }
  else if (!m_output_messages.empty())
  {
    std::string message;
    for (auto msg : m_output_messages)
    {
      message += std::string(msg);
    }
    pub_data["text/plain"] = message.c_str();
    if (!silent)
    {
      publish_execution_result(execution_counter, std::move(pub_data), nl::json({}));
    }
  }

  pqEventDispatcher::processEventsAndWait(100);

  return result;
}

//------------------------------------------------------------------------------
nl::json PVInterpreter::complete_request_impl(const std::string& code, int cursor_pos)
{
  QString qCode = QString::fromUtf8(code.c_str());
  this->m_completer->updateCompletionModel(qCode);

  nl::json result;
  for (int i = 0; m_completer->setCurrentRow(i); i++)
  {
    result["matches"].push_back(m_completer->currentCompletion().toStdString());
  }

  result["status"] = "ok";
  result["cursor_start"] = qCode.size() - m_completer->completionPrefix().size();
  result["cursor_end"] = cursor_pos;
  return result;
}

//------------------------------------------------------------------------------
nl::json PVInterpreter::inspect_request_impl(
  const std::string& vtkNotUsed(code), int vtkNotUsed(cursor_pos), int vtkNotUsed(detail_level))
{
  nl::json result;
  result["status"] = "ok";
  return result;
}

//------------------------------------------------------------------------------
nl::json PVInterpreter::is_complete_request_impl(const std::string& vtkNotUsed(code))
{
  nl::json result;
  result["status"] = "complete";
  return result;
}

//------------------------------------------------------------------------------
nl::json PVInterpreter::kernel_info_request_impl()
{
  nl::json result;
  result["implementation"] = "ParaView_kernel";
  result["implementation_version"] = "1.0.0";
  result["language_info"]["name"] = "python";
  result["language_info"]["version"] = "3.4+";
  result["language_info"]["mimetype"] = "text/x-python";
  result["language_info"]["file_extension"] = ".py";
  return result;
}

//------------------------------------------------------------------------------
void PVInterpreter::shutdown_request_impl() {}
